
var SERVER ={};
$( document ).ready(function() {
    document.addEventListener('tizenhwkey', function(e) {
        if (e.keyName == "back")
            try {
                tizen.application.getCurrentApplication().exit();
            } catch (ignore) {}
    });
    // inicializate server
    SERVER.init();
});

 SERVER.init =function() {
        //Init the b2bcontrol
        try {
            GLOBALS.b2bcontrol = window.b2bapis.b2bcontrol;
            GLOBALS.ipLFD = webapis.network.getIp();
        	//useKeys
   		 	SERVER.registerKeys();
            setTimeout(function() {
                SERVER.startNodeServer( GLOBALS.Path, GLOBALS.ServerPName);
            }, 1000);
            setTimeout(function() {
               SERVER.addJavascript('http://' + GLOBALS.ipLFD + ':' + GLOBALS.portSocket + '/socket.io/socket.io.js', 'head');
            }, 7000);
            setTimeout(function() {
                //Add the socketIO
               GLOBALS.Socket = io.connect('http://' + GLOBALS.ipLFD + ':' + GLOBALS.portSocket);
            }, 8000);
        } catch (e) {
            SERVER.logs(e);
        };
        // add eventListener for tizenhwkey
        document.addEventListener('keydown', function(e) {
            if(e.keyCode == 10009) //Return  key
            {
                try {tizen.application.getCurrentApplication().exit();}catch (ignore) {}
            }
            if(e.keyCode == 49) //KEY_1
            {
            	$("#logs").toggleClass("off")
            }
       });
}
/***
 * Starting the server
 */
SERVER.startNodeServer = function(serverPath, serverPName) {
    SERVER.logs("[startNodeServer] function call serverPath = " + serverPath + " serverPName = " + serverPName);
    var onSuccess = function() {
        SERVER.logs("[startNodeServer] success ");
        SERVER.logs("PRESS 1 TO HIDE LOGS");
    };
    var onError = function(error) {
        SERVER.logs("[startNodeServer] code :" + error.code + " error name: " + error.name + "  message " + error.message);
    };
    SERVER.logs("[startNodeServer] :: "+ GLOBALS.ipLFD+':'+GLOBALS.portSocket);
    GLOBALS.b2bcontrol.startNodeServer(serverPath, serverPName, onSuccess, onError);
 };
SERVER.addJavascript = function(jsname, pos) {
    var th =document.getElementsByTagName(pos)[0];
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', jsname);
    th.appendChild(s);
};
/**
* Register keys used in this application
*/
SERVER.registerKeys = function() {
    var usedKeys = ['1', '2', '3', '4', '0'];

    usedKeys.forEach(
        function (keyName) {
            tizen.tvinputdevice.registerKey(keyName);
        }
    );
};
SERVER.logs= function(msg){
  $("#logs").append(msg ,'<br>');
};