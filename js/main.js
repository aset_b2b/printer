var PRINTER = {
    "printerIp" : "192.168.1.221",
    "printerPort" : "80",
    "printerURL" : "http://192.168.1.221:80/StarWebPRNT/SendMessage",
    "refreshTime" : 2500,
    "canvasId" : "#test",
    "paperType" : "normal", /* Or black_mark || black_mark_and_detect_at_power_on */
    "blackmark_sensor": "front_side" /* Or back_side || hole_or_gap*/
    /*var url              = "http://172.168.1.223:80/StarWebPRNT/SendMessage";*/
};

window.onload = function() {
//	PRINTER.print();
	var canvas  = document.getElementById("test"),
	context = canvas.getContext('2d');
	context.font ='600 20px Arial';
	context.fillStyle = "red";
	context.fillText("Hello world!!", "20", "20"); 
	GLOBALS.Socket.on('test',function(data){
		PRINTER.print();
    });
     
};

PRINTER.socketio = function() {
	console.log(GLOBALS.Socket);
	GLOBALS.Socket.emit('dibujarCanvas',true);
}

/* Print Canvas function */
PRINTER.print = function(){
	console.log(PRINTER);
    var trader = new StarWebPrintTrader({url:PRINTER.printerURL, papertype:PRINTER.paperType, blackmark_sensor:PRINTER.blackmark_sensor});
    trader.onReceive = function (response) {
        var msg = '- onReceive -\n\n';
        msg += 'TraderSuccess : [ ' + response.traderSuccess + ' ]\n';
        msg += 'TraderStatus : [ ' + response.traderStatus + ',\n';

        if (trader.isCoverOpen            ({traderStatus:response.traderStatus})) {msg += '\tCoverOpen,\n';}
        if (trader.isOffLine              ({traderStatus:response.traderStatus})) {msg += '\tOffLine,\n';}
        if (trader.isCompulsionSwitchClose({traderStatus:response.traderStatus})) {msg += '\tCompulsionSwitchClose,\n';}
        if (trader.isEtbCommandExecute    ({traderStatus:response.traderStatus})) {msg += '\tEtbCommandExecute,\n';}
        if (trader.isHighTemperatureStop  ({traderStatus:response.traderStatus})) {msg += '\tHighTemperatureStop,\n';}
        if (trader.isNonRecoverableError  ({traderStatus:response.traderStatus})) {msg += '\tNonRecoverableError,\n';}
        if (trader.isAutoCutterError      ({traderStatus:response.traderStatus})) {msg += '\tAutoCutterError,\n';}
        if (trader.isBlackMarkError       ({traderStatus:response.traderStatus})) {msg += '\tBlackMarkError,\n';}
        if (trader.isPaperEnd             ({traderStatus:response.traderStatus})) {msg += '\tPaperEnd,\n';}
        if (trader.isPaperNearEnd         ({traderStatus:response.traderStatus})) {msg += '\tPaperNearEnd,\n';}

        msg += '\tEtbCounter = ' + trader.extractionEtbCounter({traderStatus:response.traderStatus}).toString() + ' ]\n';
        APP.goToSection('screen-saver');
    }

    trader.onError = function (response) {
        var msg = '- onError -\n\n';
        msg += '\tStatus:' + response.status + '\n';
        msg += '\tResponseText:' + response.responseText;
      //  APP.goToSection('screen-saver');
    }
    try {
        var canvas = $(PRINTER.canvasId)[0];
        console.log(canvas);
        if (canvas.getContext) {
            var context = canvas.getContext('2d');
            
           
            
            var builder = new StarWebPrintBuilder();
            var request = '';
            request += builder.createInitializationElement();
            request += builder.createBitImageElement({context: context, x:0, y:0, width:550, height:80});//a partir de q punto empieza a mapearse y lo otro es el tamaño del papel
            request += builder.createCutPaperElement({feed:true});
            trader.sendMessage({request:request});
        }
    }
    catch (e) {
        console.log( e.message);
    }
};
