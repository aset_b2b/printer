/**
 * @AUTHOR: juan.cortes@samsung.com
 * @description: Sample demo of webserver.
 */
var Logic = function() {
    var express = require('express');
    var app = express();
    var server = require('http').Server(app);
    var io = require('socket.io')(server);
    var pathApp = '/queuesystem';
    var portServer = 8080;
    var number="";
    app.use(express.static('public'));

    //Serves all the request which includes /images in the url from Images folder
    app.use('/img', express.static(__dirname + pathApp + '/img'));
    app.use('/font', express.static(__dirname + pathApp + '/font'));
    app.use('/css', express.static(__dirname + pathApp + '/css'));
    app.use('/js', express.static(__dirname + pathApp + '/js'));

    var bodyParser = require("body-parser");
    app.use(bodyParser.urlencoded({ extended: false }));
    //Main Get for handler the "root" request;
    app.get('/', function (req, res) {
        res.sendFile(__dirname + pathApp + '/index.html');
    });
    //Start servers
    server.listen(portServer, function () {
        console.log('Node server is running..');
    });
    //IO SOCKETS
    io.on('connection', function (socket) {
        socket.on('dibujarCanvas',function(data){
        	socket.emit('test',data);
        });
    });
   
    //Return a sample string.
return 'Loaded';
};
module.exports = Logic;
